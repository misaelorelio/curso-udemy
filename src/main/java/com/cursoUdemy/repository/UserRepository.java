package com.cursoUdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoUdemy.Entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
