package com.cursoUdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoUdemy.Entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
