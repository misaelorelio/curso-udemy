package com.cursoUdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoUdemy.Entity.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

}
