package com.cursoUdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoUdemy.Entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
